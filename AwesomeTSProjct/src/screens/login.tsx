import React from 'react';
import {
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../theme/colors';
import fonts from '../theme/fonts';
import metrics from '../theme/metrics';

const LoginScreen = ({navigation}: any) => {
  const [number, onChangeNumber] = React.useState<string>();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <View style={styles.containerContent}>
          <Text style={{...styles.title, color: colors.PRIMAY_DEEP}}>
            Tubib
          </Text>
          <Text style={styles.paragraph}>Specialised healthcare,...</Text>
          <TextInput
            style={styles.input}
            onChangeText={onChangeNumber}
            value={number}
            placeholder="Username or email"
            keyboardType="default"
          />
          <TextInput
            style={styles.input}
            onChangeText={onChangeNumber}
            value={number}
            placeholder="Password"
          />
          <Text style={styles.paragraph}>forget password?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text>Register</Text>
          </TouchableOpacity>

          <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.WHITE,
    height: metrics.screenHeight,
    // overflow: 'hidden',
  },
  containerBackground: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  containerContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
  },
  title: {
    fontSize: fonts.size.font34,
    fontWeight: fonts.weight.bold,
  },
  paragraph: {
    fontSize: fonts.size.font16,
  },
  containerCard: {
    width: '100%',
    height: '45%',
    marginVertical: 50,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '100%',
  },
});

export default LoginScreen;
