import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../theme/colors';
import fonts from '../theme/fonts';
import metrics from '../theme/metrics';

const IndexPage = ({navigation}: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <View style={{position: 'relative', height: '5%'}}>
          <View style={styles.containerBackground}>
            <View style={styles.circle1} />
            <View style={styles.circle2} />
            <View style={styles.circle3} />
          </View>
        </View>
        <View style={styles.containerContent}>
          <View>
            <Text style={{...styles.title, color: colors.PRIMAY_DEEP}}>
              Virtual
            </Text>
            <Text style={{...styles.title, color: colors.WHITE}}>
              Ecosystem.
            </Text>
            <Text style={styles.paragraph}>
              Specialised healthcare, on a single tech platform, simplifying
              access for anyone, anywhere
            </Text>
          </View>
          <View style={styles.containerCard}>
            <View style={styles.bgCircle} />
            <View style={styles.bgCircle2} />
            <View style={{...styles.card, ...styles.cardLeft}} />
            <View style={{...styles.card, ...styles.cardRight}} />
            <TouchableOpacity
              onPress={() => navigation.push('Login')}
              style={styles.getStarted}>
              <View style={styles.start} />
              <View>
                <Text style={styles.textStart}>Get</Text>
                <Text style={styles.textStart}>Started</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.PRIMARY,
    height: metrics.screenHeight,
    // overflow: 'hidden',
  },
  containerBackground: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  containerContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
  },
  title: {
    fontSize: fonts.size.font34,
    fontWeight: fonts.weight.bold,
  },
  paragraph: {
    fontSize: fonts.size.font16,
  },
  containerCard: {
    width: '100%',
    height: '45%',
    marginVertical: 50,
  },
  card: {
    backgroundColor: colors.WHITE,
    height: 240,
    width: '45%',
    position: 'absolute',
    borderRadius: 20,
    shadowColor: '#171717',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  cardLeft: {left: 0},
  cardRight: {right: 0, bottom: 0},
  circle1: {
    borderWidth: 5,
    borderColor: 'rgba(255, 255, 255, .3)',
    height: 120,
    width: 120,
    borderRadius: 100,
    right: -18,
    top: -26,
    position: 'absolute',
  },
  circle2: {
    backgroundColor: colors.PRIMAY_DEEP,
    height: 60,
    width: 60,
    borderRadius: 100,
    right: 89,
    top: 64,
    position: 'absolute',
  },
  circle3: {
    backgroundColor: colors.PRIMARY_LIGHT,
    height: 10,
    width: 10,
    borderRadius: 100,
    right: 65,
    top: 132,
    position: 'absolute',
  },
  getStarted: {
    position: 'absolute',
    bottom: -35,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  start: {
    width: 70,
    height: 70,
    backgroundColor: colors.PRIMAY_DEEP,
    borderRadius: 70,
    marginRight: 15,
  },
  textStart: {
    fontSize: fonts.size.font17,
    fontWeight: fonts.weight.bold,
    color: colors.WHITE,
  },
  bgCircle: {
    width: metrics.screenwidth / 1.7,
    height: metrics.screenwidth / 1.7,
    borderRadius: metrics.screenwidth,
    borderWidth: 1,
    borderColor: colors.GREY_LIGHT,
    position: 'absolute',
    right: metrics.screenwidth / 10,
    top: metrics.screenwidth / 10,
  },
  bgCircle2: {
    width: metrics.screenwidth / 1.7,
    height: metrics.screenwidth / 1.7,
    borderRadius: metrics.screenwidth,
    borderWidth: 1,
    borderColor: colors.GREY_LIGHT,
    position: 'absolute',
    right: metrics.screenwidth / 10 + 10,
    top: metrics.screenwidth / 10,
  },
});

export default IndexPage;
